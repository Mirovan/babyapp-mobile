import 'package:babyapp/drawer_menu.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return new Scaffold(
            appBar: new AppBar(
                title: new Text('Login page app bar'),
            ),
            body: new Text('Login page'),
            drawer: new DrawerMenu()
        );
    }
}