import 'package:babyapp/drawer_menu.dart';
import 'package:flutter/material.dart';

class SearchPage extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return new Scaffold(
            appBar: new AppBar(
                title: new Text('Login page app bar'),
            ),
            body: new Text('Search page'),
            drawer: new DrawerMenu()
        );
    }
}