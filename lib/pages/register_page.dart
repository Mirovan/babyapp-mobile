import 'package:babyapp/drawer_menu.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return new Scaffold(
            appBar: new AppBar(
                title: new Text('Register page app bar'),
            ),
            body: new Text('Register page'),
            drawer: new DrawerMenu()
        );
    }
}