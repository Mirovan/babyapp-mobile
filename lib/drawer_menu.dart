import 'package:babyapp/pages/login_page.dart';
import 'package:babyapp/pages/register_page.dart';
import 'package:babyapp/pages/search_page.dart';
import 'package:flutter/material.dart';

class DrawerMenu extends Drawer {
    @override
    Widget build(BuildContext context) {
        return new Drawer(
            child: new ListView(
                children: <Widget>[
                    new DrawerHeader(
                        child: new Text('Drawer Header 111'),
                        decoration: new BoxDecoration(
                            color: Colors.blue
                        ),
                    ),
                    new ListTile(
                        title: new Text('Search babysitters'),
                        onTap: () {
                            Navigator.pop(context);
                            Navigator.push(context, new MaterialPageRoute(builder: (context) => new SearchPage()));
                        }
                    ),
                    new ListTile(
                        title: new Text('Login'),
                        onTap: () {
                            Navigator.pop(context);
                            Navigator.push(context, new MaterialPageRoute(builder: (context) => new LoginPage()));
                        }
                    ),
                    new ListTile(
                        title: new Text('Register'),
                        onTap: () {
                            Navigator.pop(context);
                            Navigator.push(context, new MaterialPageRoute(builder: (context) => new RegisterPage()));
                        }
                    ),
                ],
            ),
        );
    }
}